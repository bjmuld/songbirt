from setuptools import setup
import os
import re
import inspect
import shutil
from fnmatch import fnmatch

packagename = 'songbirt'
short_description = "Songbirt is a web-server for browsing and viewing repertoires during performance"

# get the full path to this file :
thispath = os.path.abspath(os.path.realpath(os.path.split(inspect.stack()[0][1])[0]))
readmefile = os.path.join(thispath, 'README.md')
initfile = os.path.join(thispath, 'src', packagename, '__init__.py')

# read actual long description from /README.md
if os.path.exists(readmefile):
    with open(readmefile, 'r') as f:
        long_description = f.read()
else:
    long_description = short_description

# read version
if os.path.isfile(initfile):
    with open(initfile, 'r') as f:
        for line in f.readlines():
            scraped = re.search(r'\s*__version__\s*=\s*[\"\'](\S+)[\"\']', line)
            if scraped:
                version = scraped.group(1).strip('\'\"').strip()
                break

setup(

    name=packagename,
    version=str(version),

    python_requires='>=2.7',
    setup_requires=['setuptools-scm', ],
    use_scm_version=True,
    install_requires=['bottle', 'markdown', 'ttkthemes', 'pynput', 'RPi.GPIO'],
    #tests_require=["pytest"],
    # extras_require={
    #         'dev': [
    #             'pytest',
    #             'tox',
    #         ]
    #     },
    packages={'songbirt': 'src/songbirt',
              'squid': 'src/squid'},
    package_dir={'': 'src'},
    entry_points={
        'console_scripts': [
            'songbirt = songbirt.songbirt:main',
        ],
        'gui_scripts': [
            'songbirt = songbirt.songbirt:main',
        ],
    },

    include_package_data=True,

    # metadata to display on PyPI
    author="Barry Muldrey",
    author_email="barry@muldrey.net",
    description=short_description,
    long_description=long_description,
    long_description_content_type='text/markdown',
    license="GPLv3",
    keywords="music",
    url="http://gitlab.com/bjmuld/songbirt/",
    project_urls={
        "Bug Tracker": "https://gitlab.com/bjmuld/songbirt/issues",
        "Documentation": "https://gitlab.com/bjmuld/songbirt",
    },

    classifiers=[
        # 'Development Status :: 3 - Alpha',
        # 'Environment :: Console',
        # 'Intended Audience :: End Users/Desktop',
        # 'Intended Audience :: Developers',
        # 'Intended Audience :: Science/Research',
        # 'Intended Audience :: Education',
        # 'License :: OSI Approved :: GNU Affero General Public License v3',
        # 'Operating System :: MacOS',
        # 'Operating System :: Microsoft :: Windows',
        # 'Operating System :: POSIX',
        # 'Natural Language :: English',
        # 'Programming Language :: Python',
        # 'Topic :: Scientific/Engineering',
        # 'Topic :: Scientific/Engineering :: Visualization',
        # 'Topic :: Utilities',
    ],
)
