#! /usr/bin/env python3

import os
import threading
import socket
import time
import base64
import webbrowser
import argparse
import urllib.request as urllib
import requests
import json
import bottle
import markdown
import signal
import tempfile
import shutil
import subprocess

try:
    from squid.squid import Squid, PURPLE, RED, OFF
    from squid.button import Button
    from pynput.keyboard import Key, Controller
except:
    pass

from contextlib import closing
import tkinter
from tkinter import ttk
from ttkthemes import ThemedTk

PORT = None
SERVER = None
T_SERV = None
T_GUI = None
WINDOW = None
LOCAL_DB = None
IDLY = None
SCLK = None
METRONOME = None
BUTTON_LISTENER = None

GITLAB_PREFIX = "https://gitlab.com/api/v4/"
met_stop = threading.Event()
keylisten_stop = threading.Event()


class UnknownSourceError(ValueError):
    pass

class ButtonListener(object):
    def __init__(self):
        dbmsg('creating button listener')
        # use 'squid' library to monitor pins
        self.b2 = Button(8)
        self.b1 = Button(7)

        # use pynput library to simulate keyboard button presses
        # 	pynput.Key.page_up
        # 	pynput.Key.page_down
        self.vkeyboard = Controller()

        self.thread = threading.Thread(target=self.listen)

    def start(self):
        dbmsg('starting button listener thread')
        self.thread.start()

    def stop(self):
        dbmsg('trying to stop button listener thread')
        if hasattr(self, 'thread') and self.thread.is_alive():
            keylisten_stop.set()
            self.thread.join()
            dbmsg('stopped button listener thread')
        else:
            dbmsg('no button listener thread to stop')

    def listen(self):
        dbmsg('hello from inside the button listener thread')
        while not keylisten_stop.is_set():
            if self.b1.is_pressed():
                print("Button1 press")
                self.vkeyboard.press(Key.page_up)
                self.vkeyboard.release(Key.page_up)

            if self.b2.is_pressed():
                print("Button2 pressed")
                self.vkeyboard.press(Key.page_down)
                self.vkeyboard.release(Key.page_down)

            time.sleep(0.01)


class Metronome(object):
    def __init__(self, bpm, meter):
        self.set(bpm, meter)
        self.thread = threading.Thread(target=self.beat, daemon=True)

    def start(self):
        self.thread.start()

    def stop(self):
        met_stop.set()
        if hasattr(self, 'thread') and self.thread.is_alive():
            self.thread.join()
        met_stop.clear()
        self.thread = threading.Thread(target=self.beat, daemon=True)

    def set(self, bpm, meter):
        self.stop()
        self.bpm = bpm
        self.meter = meter

    def beat(self):
        rgb = Squid(18, 23, 24)
        period = 60.0 / self.bpm
        ontime = period / 3.0
        offtime = period - ontime
        bid = 0
        while not met_stop.is_set():
            if bid == 0:
                rgb.set_color(PURPLE)
            else:
                rgb.set_color(RED)

            time.sleep(ontime)
            rgb.set_color(OFF)
            time.sleep(offtime)
            bid += 1
            bid %= self.meter


def asurl(str):
    return str.lstrip('/').replace(' ', '%20').replace('/', '%2F')


class Gitlab_URL(object):
    def __init__(self, url):
        url = url.split('gitlab.com')[1].lstrip('/')
        self.url = asurl(url)
        self.repo = asurl('/'.join(url.split('/')[:2]))
        self.file = asurl('/'.join(url.split('/')[2:]))


class PathReq(object):
    def __init__(self, req):

        self.string = req
        parts = self.string.lstrip('/').rstrip('/').split(os.sep)
        self.parts = parts
        self.artist = None
        self.album = None
        self.item = None

        if '.' in self.parts[-1]:

            self.item = parts[-1]

            if len(self.parts) == 2:
                self.artist = parts[0]
            elif len(self.parts) == 3:
                self.artist = parts[0]
                self.album = parts[1]

        else:
            if len(self.parts) == 1:
                self.artist = parts[0]
            elif len(self.parts) == 2:
                self.artist = parts[0]
                self.album = parts[1]

    def is_item(self):
        if self.item:
            return True
        else:
            return False

    def asdict(self):
        return {'artist': self.artist,
                'album': self.album,
                'item': self.item}


class SongTree(object):
    def __init__(self, dict_tree):
        self.tree = dict_tree

    def select(self, artist=None, album=None, item=None):

        if artist:
            result = filter(lambda x: x['artist'] == artist, self.tree)
        else:
            result = self.tree

        if album:
            result = filter(lambda x: x['album'] == album, result)

        if item:
            ss = os.path.splitext(item)[0]
            result = filter(lambda x: ss == os.path.splitext(x['item'])[0], result)

        return list(result)


# Quickly grab an open port:
def find_free_port():
    with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as s:
        s.bind(('', 0))
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        return s.getsockname()[1]


# walk root directory; build DB
def build_local_db():
    db = []

    for r, ds, fs in os.walk(ARGS.serve_path, topdown=True, followlinks=True):

        for d in ds:
            if d.startswith('.'):
                ds.remove(d)

        if fs:
            info = os.path.relpath(r, ARGS.serve_path).lstrip('/').rstrip('/').split(os.sep)

        for f in fs:
            file = os.path.join(r, f)  # .replace(' ', '\ ')
            db.append({
                'artist': info[0],
                'album': info[1] if len(info) > 1 else '',
                'item': os.path.basename(file),
                'file': file,
            })

    return SongTree(db)


def build_gitlab_db(url):
    db = []
    gurl = Gitlab_URL(url)
    tree = requests.get(GITLAB_PREFIX + 'projects/' + gurl.repo + '/repository/tree?recursive=1&per_page=999')
    if tree.status_code == 404:
        return None
    elif tree.status_code == 200:
        tree = json.loads(tree.content.decode())
        for item in tree:
            if item['type'] == 'blob':
                p = PathReq(item['path'])
                db.append(
                    {
                        'artist': p.artist if p.artist else '',
                        'album': p.album if p.album else '',
                        'item': p.item,
                        'gitlab-item': item,
                    }
                )

    return SongTree(db)


def build_github_db(url):
    raise NotImplementedError


def astable(dblist):
    table = [
        '<table id="myTable" align="center">'       '\n'
        '<tr>'                                      '\n'
        '    <th onclick="sortTable(0)">Artist</th>''\n'
        '    <th onclick="sortTable(1)">Album</th>' '\n'
        '    <th onclick="sortTable(2)">Item</th>' '\n'
        '</tr>'                                     '\n'
    ]

    for r in dblist:
        table.append(tmpl_tablerow_3c.render(
            col0=aslink(artist=r['artist']),
            col1=aslink(artist=r['artist'], album=r['album']),
            col2=aslink(artist=r['artist'], album=r['album'], item=r['item']),
        ))
    table.append('</table>')

    return '\n'.join(table)


def aslink(artist=None, album=None, item=None):
    if not artist and not album and not item:
        link = '/'
        text = 'HOME'

    elif artist and not album and not item:
        if album is None:
            link = '/' + artist
            text = artist
        elif album == '':
            link = ''
            text = ''

    elif artist and album and not item:
        link = '/' + artist + '/' + album
        text = album

    elif artist and album and item:
        link = '/' + artist + '/' + album + '/' + item
        text = item.split('.')[0]

    elif artist and not album and item:
        link = '/' + artist + '/' + item
        text = item.split('.')[0]

    else:
        raise ValueError

    return '<a href=\"/browse/{}\">{}</a>'.format(asurl(link), text)


def dbmsg(string):
    if ARGS.debug:
        print(string)
    else:
        pass


def present_table(artist=None, album=None):

    if METRONOME:
        METRONOME.stop()

    if ARGS.serve_path:
        dbmsg('serving from ARGS.serve_path: {}'.format(ARGS.serve_path))
        bottle.response.set_cookie('mainurl', '.')
        db = build_local_db()

    elif ARGS.url:
        dbmsg('serving from ARGS.url: {}'.format(ARGS.url))
        if 'gitlab.com' in ARGS.url:
            db = build_gitlab_db(ARGS.url)
        elif 'github.com' in ARGS.url:
            db = build_github_db(ARGS.url)

    else:
        source = bottle.request.get_cookie('mainurl')
        dbmsg('serving from cookie: {}'.format(source))

        if 'gitlab.com' in source:
            dbmsg('found github cookie')
            db = build_gitlab_db(source)
        elif 'github.com' in source:
            dbmsg('found githib cookie')
            db = build_github_db(source)
        else:
            dbmsg('no valid cookie to serve from')
            return bottle.template('startup')

    items = db.select(artist=artist, album=album)
    table = astable(items)

    return bottle.template('main',
                           artist=artist,
                           album=album,
                           content=tmpl_table_container.render(table=table),
                           song=None,
                           )


def present_item(item, artist=None, album=None):

    if ARGS.serve_path:
        dbmsg('serving from ARGS.serve_path: {}'.format(ARGS.serve_path))
        source = ARGS.serve_path

    elif ARGS.url:
        dbmsg('serving from ARGS.url: {}'.format(ARGS.url))
        source = ARGS.url

    else:
        source = bottle.request.get_cookie('mainurl')

    if source == '.':
        db = build_local_db()
    elif 'gitlab.com' in source:
        db = build_gitlab_db(source)
    elif 'github.com' in source:
        db = build_github_db(source)
    else:
        return error_page()

    cands = db.select(artist=artist, album=album, item=item)

    if len(cands) != 1:
        return error_page()

    type = os.path.splitext(item)[1]

    if type in ['.md', '.html', '.txt']:

        try:
            fc = file_contents(cands[0])
        except UnknownSourceError:
            return error_page()

        try:
            tempo = fc.split('[metronome:]')[1].split('\n')[0].strip()
            if tempo and METRONOME:
                if '/' in tempo:
                    bpm, meter = tempo.split('/')
                else:
                    bpm = tempo
                    meter = 4
                METRONOME.set(float(bpm), float(meter))
                METRONOME.start()
        except:
            pass

        if type == '.md':
            contents = tmpl_text_container.render(text=markdown.markdown(fc))

        elif type == '.html':
            contents = tmpl_text_container.render(text=fc)

        elif type == '.txt':
            contents = tmpl_text_container.render(text=wrap_html(fc, 'pre'))

        return bottle.template('main',
                               artist=artist,
                               album=album,
                               song=item,
                               content=contents,
                               )

    elif type == '.pdf':

        if not source or source == '.':
            return bottle.template(
                'pdf',
                artist=artist,
                album=album,
                song=item,
                fileurl=urllib.pathname2url(
                    '/data/{}/{}/{}'.format(artist, album, item) if album
                    else '/data/{}/{}'.format(artist, item)),
            )

        elif 'gitlab' in source:
            gurl = Gitlab_URL(source)
            pdf_data = requests.get(
                GITLAB_PREFIX + 'projects/' \
                + gurl.repo + '/repository/files/' \
                + asurl(cands[0]['gitlab-item']['path']) + '/raw?ref=master'
            ).content

            try:
                tf = tempfile.NamedTemporaryFile(dir='tmp', suffix='.pdf', delete=False)
                tf.write(pdf_data)

                return bottle.template(
                    'pdf',
                    artist=artist,
                    album=album,
                    song=item,
                    fileurl='/tmp/' + os.path.split(tf.name)[1],
                )

            except:
                os.remove(tf.name)

    else:
        return error_page()


def file_contents(item):

    if ARGS.serve_path:
        dbmsg('reading file from ARGS.serve_path: {}'.format(ARGS.serve_path))
        fp = item['file']
        # it's a local file
        if not os.path.isfile(fp):
            fp = os.path.join(ARGS.serve_path, fp)

        try:
            with open(fp, 'r') as f:
                contents = f.read()
        except:
            contents = None

        return contents

    elif ARGS.url:
        dbmsg('serving from ARGS.url: {}'.format(ARGS.url))

        if 'gitlab.com' in ARGS.url:
            gurl = Gitlab_URL(ARGS.url)
            file = requests.get(GITLAB_PREFIX
                                + 'projects/' + gurl.repo + '/repository/files/'
                                + asurl(item['gitlab-item']['path'])
                                + '?ref=master'
                                )
            if file.status_code != 200:
                return None
            return base64.b64decode(json.loads(file.content.decode())['content']).decode()

        elif 'github.com' in ARGS.url:
            db = build_github_db(ARGS.url)

    else:
        source = bottle.request.get_cookie('mainurl')
        if not source:
            raise UnknownSourceError
        dbmsg('serving from cookie: {}'.format(source))

        if 'gitlab.com' in source:
            gurl = Gitlab_URL(source)
            file = requests.get(GITLAB_PREFIX
                                + 'projects/' + gurl.repo + '/repository/files/'
                                + asurl(item['gitlab-item']['path'])
                                + '?ref=master'
                                )
            if file.status_code != 200:
                return None
            return base64.b64decode(json.loads(file.content.decode())['content']).decode()

        elif 'github.com' in source:
            db = build_github_db(source)

        else:
            raise UnknownSourceError


def error_page():
    return bottle.template('main',
                           artist='Error',
                           album=None,
                           song='ERROR',
                           )


def wrap_html(html, entity_type, kwdict=None):
    if kwdict:
        open = '<{} {}>'.format(entity_type, ' '.join(['{}=\"{}\"'.format(k, v) for k, v in kwdict.items()]))
    else:
        open = '<{}>'.format(entity_type)
    return open + '\n' + html + '\n' + '</{}>'.format(entity_type)


def launch_browser():
    webbrowser.open('localhost:{}'.format(PORT), new=1, autoraise=True)


def set_cookie():
    if not bottle.request.get_cookie('mainurl'):

        if ARGS.serve_path:
            bottle.response.set_cookie('mainurl', '.')

        elif ARGS.url and ('github' in ARGS.url or 'gitlab' in ARGS.url):
            bottle.response.set_cookie('mainurl', ARGS.url)

        else:
            raise UnknownSourceError


def load_site():

    @SERVER.route('/')
    def root():
        try:
            set_cookie()
        except UnknownSourceError:
            return bottle.template('startup')

        return bottle.redirect('/browse/')

    @SERVER.route('/browse')
    def redirect():
        try:
            set_cookie()
        except UnknownSourceError:
            return bottle.template('startup')

        return present_table()

    @SERVER.route('/browse/')
    def redirect():
        try:
            set_cookie()
        except UnknownSourceError:
            return bottle.template('startup')

        return present_table()

    @SERVER.route('/browse/<path:path>')
    def browse(path):
        try:
            set_cookie()
        except UnknownSourceError:
            return bottle.template('startup')

        req = PathReq(path)

        if req.is_item():
            return present_item(artist=req.artist, album=req.album, item=req.item)

        elif req.artist and not req.album:
            return present_table(artist=req.artist)

        elif req.artist and req.album:
            return present_table(artist=req.artist, album=req.album)

    @SERVER.route('/setup-submit', method='POST')
    def receive():
        url = bottle.request.forms.get('pathtoserve')
        if url.startswith('gitlab.com') or url.startswith('github.com'):
            bottle.response.set_cookie('mainurl', url)
        else:
            bottle.response.set_cookie('mainurl', '.')
        return bottle.redirect('/browse/')

    @SERVER.route('/static/<path:path>')
    def static(path):
        return bottle.static_file(path, root='./static')

    @SERVER.route('/tmp/<name>')
    def tmppdf(name):
        try:
            return bottle.static_file(name, root='tmp/')
        except:
            pass
        finally:
            os.remove(os.path.join('.', 'tmp', name))

    @SERVER.route('/data/<path:path>')
    def data(path):
        # this is a request for file contents
        req = PathReq(path)
        source = bottle.request.get_cookie('mainurl')

        if not source or source == '.':
            db = build_local_db()
        elif 'gitlab.com' in source:
            db = build_gitlab_db(source)
        elif 'github.com' in source:
            db = build_github_db(source)
        else:
            return error_page()

        cands = db.select(artist=req.artist, album=req.album, item=req.item)

        if len(cands) != 1:
            return error_page()

        file = cands[0]['file']
        return bottle.static_file(filename=os.path.relpath(file, ARGS.serve_path), root=ARGS.serve_path)


def build_gui():
    # graphical stuff
    WINDOW.title('Songbirt')
    # WINDOW.geometry('800x100')
    WINDOW.configure(bg='gray17')
    # WINDOW.attributes('-topmost', True)
    f1 = ttk.Frame(WINDOW)
    ttk.Button(f1, text='Open Browser', command=launch_browser).pack(side=tkinter.LEFT, expand=False, padx=5, pady=5)
    ttk.Button(f1, text="Quit", command=die).pack(side=tkinter.LEFT, expand=False, padx=5, pady=5)
    f1.pack(side=tkinter.RIGHT, expand=False)
    # ttk.Button(WINDOW, text='Page Up', command=None).pack(side=tkinter.LEFT, expand=True, fill='both')
    # ttk.Button(WINDOW, text='Page Down', command=None).pack(side=tkinter.LEFT, expand=True, fill='both')


def init():
    global PORT, SERVER, T_SERV, T_GUI, LOCAL_DB, METRONOME, BUTTON_LISTENER

    # handle clargs
    if not ARGS.port:
        PORT = find_free_port()
    else:
        PORT = ARGS.port

    # path
    if ARGS.serve_path:
        assert os.path.isdir(ARGS.serve_path), 'invalid path specified with -P[--path] option: {}'.format(ARGS.serve_path)

    # site
    SERVER = bottle.Bottle()
    load_site()
    T_SERV = threading.Thread(target=boot_server, name='songbird_server', kwargs={'host': 'localhost', 'port': PORT},
                              daemon=True)

    # gui
    if not ARGS.nogui:
        T_GUI = threading.Thread(target=boot_gui, name='songbirt_gui', daemon=True)

    # Metronome and buttons
    if 'Squid' in globals():
        dbmsg('creating metronome object')
        METRONOME = Metronome(120, 4)
        dbmsg('creating button-listener object')
        BUTTON_LISTENER = ButtonListener()


def start():
    global IDLY, SCLK

    # set signal listeners:
    signal.signal(signal.SIGABRT, die)
    signal.signal(signal.SIGQUIT, die)
    signal.signal(signal.SIGTERM, die)
    signal.signal(signal.SIGINT, die)
    signal.signal(signal.SIGHUP, die)

    try:

        # try to start raspberry-pi button listener
        try:
            dbmsg('trying to start the button listener')
            BUTTON_LISTENER.start()
        except:
            pass

        # start server
        T_SERV.start()

        if not ARGS.nogui:
            # screensaver stuff
            IDLY = subprocess.check_output(
                ['gsettings', 'get', 'org.gnome.desktop.session', 'idle-delay']).decode().strip().split('uint32')[
                1].strip()
            SCLK = subprocess.check_output(
                ['gsettings', 'get', 'org.gnome.desktop.screensaver', 'lock-enabled']).decode().strip()
            subprocess.check_call(['gsettings', 'set', 'org.gnome.desktop.session', 'idle-delay', str(0)])
            subprocess.check_call(['gsettings', 'set', 'org.gnome.desktop.screensaver', 'lock-enabled', 'false'])

            # start gui
            T_GUI.start()

            while T_GUI.is_alive():
                T_GUI.join(1)

            die()

        while T_SERV.is_alive():
            T_SERV.join(1)

        die()

    except Exception as e:
        die()
        raise e


def boot_gui():
    global WINDOW
    WINDOW = ThemedTk(theme='equilux')
    build_gui()
    WINDOW.mainloop()


def boot_server(host='localhost', port=56477):

    os.chdir(os.path.split(__file__)[0])

    if os.path.isdir('tmp'):
        shutil.rmtree('tmp')

    if not os.path.isdir('tmp'):
        os.mkdir('tmp')

    try:
        SERVER.run(host=host, port=port)

    except:
        die()


def diemsg(msg):
    raise NotImplementedError
    # print(msg)
    # try:
    #     die()
    # finally:
    #     raise RuntimeError(msg)


def die(*args, **kwargs):

    # try:
    #     SERVER.close()
    # except Exception:
    #     pass
    #
    # try:
    #     T_SERV.join(0.5)
    # except Exception:
    #     pass
    #
    # try:
    #     WINDOW.destroy()
    # except Exception:
    #     pass

    if not ARGS.nogui:
        subprocess.check_call(['gsettings', 'set', 'org.gnome.desktop.session', 'idle-delay', str(IDLY)])
        subprocess.check_call(['gsettings', 'set', 'org.gnome.desktop.screensaver', 'lock-enabled', str(SCLK)])

    try:
        METRONOME.stop()
    except:
        pass

    try:
        BUTTON_LISTENER.stop()
    except:
        pass

    raise SystemExit


def main():
    global ARGS, UARGS
    parser = argparse.ArgumentParser()
    parser.add_argument('--serve-path', '-P')
    parser.add_argument('--port', '-p', action='store', default=0)
    parser.add_argument('--public', action='store_true')
    parser.add_argument('--url', '-u', action='store')
    parser.add_argument('--debug', action='store_true')
    parser.add_argument('--nogui', action='store_true')
    ARGS, UARGS = parser.parse_known_args()
    init()
    start()


tmpl_tablerow_3c = bottle.SimpleTemplate(
    '<tr>'                      '\n'
    '    <td>{{!col0}}</td>'     '\n'
    '    <td>{{!col1}}</td>'     '\n'
    '    <td>{{!col2}}</td>'    '\n'
    '</tr>')

tmpl_text_container = bottle.SimpleTemplate("""
<div class="textcontainer">
{{!text}}
</div>
""")

tmpl_table_container = bottle.SimpleTemplate("""
<div class="tablecontainer">
{{!table}}
</div>
""")

html_iframe_scpt = """
<script">
</script>
"""

if __name__ == '__main__':
    main()
