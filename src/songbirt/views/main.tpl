<!DOCTYPE html>
<html>
<head>
    <title>SongbirT</title>
    <meta name="SongbirT" content="width=device-width, initial-scale=1, maximum-scale=1">

    <script type="text/javascript" src="/static/myjava.js"></script>
    <link rel="icon" href="/static/favicon.ico">
    <link rel="stylesheet" type="text/css" href="/static/mystyle.css">
    % if 'scripts' in locals() and scripts:
        {{!scripts}}
    %end
</head>
<body>

    % if 'artist' in locals() and artist:
    % include('navbar.tpl', artist=artist, album=album, song=song)
    % end

    % if 'content' in locals() and content:
        <div class="basecontainer">
            {{!content}}
        </div>
    % end

    <div id="buttons">
        <table>
        <tr>
            <button class="touchbutton" onclick="scrollWin(0, -600)">PAGE UP</button>
            <button class="touchbutton" onclick="scrollWin(0, 600)">PAGE DOWN</button>
        </tr>
        </table>
    </div>

</body>
</html>
