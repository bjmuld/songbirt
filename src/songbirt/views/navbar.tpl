<div id="navbar">
    <ul>

        <li><a class="active touchbutton" href="/browse/">Home</a></li>
        <li><a class="touchbutton" href="/browse/{{artist}}">{{artist}}</a></li>

        % if 'album' in locals() and album:
            <li><a class="touchbutton" href="/browse/{{artist}}/{{album}}">{{album}}</a></li>
        % end

        % if 'song' in locals() and song:
            % if 'album' in locals() and album:
                <li><a class="touchbutton" href="/browse/{{artist}}/{{album}}/{{song}}">{{song}}</a></li>
            % else:
                <li><a class="touchbutton" href="/browse/{{artist}}/{{song}}">{{song}}</a></li>
            % end
        % end

    </ul>
</div>