<!DOCTYPE html>
<html>
<head>
    <title>SongbirT</title>
    <meta name="SongbirT" content="width=device-width, initial-scale=1, maximum-scale=1">

    <script type="text/javascript" src="/static/myjava.js"></script>
    <link rel="icon" href="/static/favicon.ico">
    <link rel="stylesheet" type="text/css" href="/static/mystyle.css">

</head>
<body>

    % include('navbar.tpl', artist=artist, album=album, song=song)

    <object id="myviewer"
            data="/static/pdf.js/web/viewer.html?file={{fileurl}}"
            width="100%" height="90%" style="margin-top:65px">
        <p>Alternative text - include a link <a href="{{fileurl}}">to the PDF!</a></p>
    </object>

    <div id="buttons">
        <button class="touchbutton" onclick="scrollViewer(0, 600, document.getElementById('myviewer'))">Scroll down</button>
        <button class="touchbutton" onclick="scrollViewer(0, -600, document.getElementById('myviewer'))">Scroll up</button>
    </div>

</body>
</html>